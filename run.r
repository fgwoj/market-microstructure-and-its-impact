book.total_volumes <- function(book) {
    # Arguments:
    #   book - A list containing "ask" and "bid", each of which are dataframes
    #       containing the collection of limit orders.
    #
    # Returns:
    #   The total volume in the book.

    volume_ask <- book$ask[["size"]]
    volume_bid <- book$bid[["size"]]
    total <- list(ask = sum(volume_ask), bid = sum(volume_bid))

    return(total)
}

book.best_prices <- function(book) {
    # Arguments:
    #   book - A list containing "ask" and "bid", each of which are dataframes
    #       containing the collection of limit orders.
    #
    # Returns:
    #   A list with "ask" and "bid", the values of which are the best prices in
    #       the book.
    
    volume_ask <- book$ask[["price"]]
    volume_bid <- book$bid[["price"]]

    z <- volume_ask[order(volume_ask, decreasing = FALSE)]
    y <- volume_bid[order(volume_bid, decreasing = TRUE)]

    f <- c(z)
    g <- c(y)

    x <- list(ask = f[1], bid = g[1])

    return(x)
}

book.midprice <- function(book) {
    # Arguments:
    #   book - A list containing "ask" and "bid", each of which are dataframes
    #       containing the collection of limit orders.
    #
    # Returns:
    #   The midprice of the book.
    volume_ask <- book$ask[["price"]]
    volume_bid <- book$bid[["price"]]

    z <- volume_ask[order(volume_ask, decreasing = FALSE)]
    y <- volume_bid[order(volume_bid, decreasing = TRUE)]

    f <- c(z)
    g <- c(y)

    return((f[1] + g[1]) / 2)
}

book.spread <- function(book) {
    # Arguments:
    #   book - A list containing "ask" and "bid", each of which are dataframes
    #       containing the collection of limit orders.
    #
    # Returns:
    #   The spread of the book.

    volume_ask <- book$ask[["price"]]
    volume_bid <- book$bid[["price"]]

    z <- volume_ask[order(volume_ask, decreasing = FALSE)]
    y <- volume_bid[order(volume_bid, decreasing = TRUE)]

    f <- c(z)
    g <- c(y)

    return(abs(f[1] - g[1]))
}

book.add <- function(book, message) {
    # Arguments:
    #   book - A list containing "ask" and "bid", each of which are dataframes
    #       containing the collection of limit orders.
    #   message - A list containing "oid", "side", "price" and "size" entries.
    #
    # Returns:
    #   The updated book.
    # S (SELL) = ask
    # B (BUY) = bid 

    f <- data.frame("side" = message$side,
                "oid" = message$oid, 
                "price" = message$price, 
                "size" = message$size )
      
    if(f$side == "S"){
        f <- data.frame("oid" = message$oid, 
                        "price" = message$price, 
                        "size" = message$size 
                        )   
        i <- 0
        g <- TRUE               
        while(g){ 
            i <- i + 1
            if(is.na(book$bid$price[i]) == FALSE){
                if(book$bid$price[i] >=  f$price){
                    book$bid$size[i] <- book$bid$size[i] - f$size

                    if(book$bid$size[i] <= 0){
                        book$bid <- book$bid[-c(i), ]
                    }

                }else if (book$bid$price[i] ==  f$price){
                    book$bid <- book$bid[-c(i), ]

                }else{
                    book$ask[nrow(book$ask) + 1, ] <- f
                    break
                }
            }else{
                break
            }             

        }
        
    }else if (f$side == "B") {
        f <- data.frame("oid" = message$oid, 
                        "price" = message$price, 
                        "size" = message$size 
                        )
        i <- 0
        g <- TRUE
        while(g){
            i <- i + 1
            if(is.na(book$ask$price[i])== FALSE){
                if(book$ask$price[i] <  f$price){
                    w <- f$size - book$ask$size[i]
                    if(w > 0){
                        f$size <- w
                        book$bid[nrow(book$bid) + 1, ] <- f
                        book$ask <- book$ask[-c(i), ]
                    }else{
                        book$ask <- book$ask[-c(i), ]
                    }
                    
                }else if (book$ask$price[i] ==  f$price){
                    book$ask <- book$ask[-c(i), ]
                }else{
                    book$bid[nrow(book$bid) + 1, ] <- f
                    g <- FALSE
                }
            }else{
                g <- FALSE
            }

        }

    }

    return(book)